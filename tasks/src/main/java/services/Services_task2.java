package services;

import dao.DAO;
import model.Operation;

import java.util.List;

public class Services_task2 extends Services {

    private DAO operationDAO;

    public Services_task2(DAO operationDAO) {
        this.operationDAO = operationDAO;
    }

    public List<Operation> getOperationAsList() {
        return operationDAO.getAllAsList();
    }

    public void loadOperation(String filePath) {
        operationDAO.loadFromFile(filePath);
    }
}
