package services;

import model.Operation;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class DataGenerationService {

    private Services_task1 servicesTask1;

    public DataGenerationService(Services_task1 servicesTask1) {
        this.servicesTask1 = servicesTask1;
    }

    public void run(String pointOfSaleListFilePath, int operationsCount, String... outFileNames) {
        try {
            doIt(pointOfSaleListFilePath, operationsCount, outFileNames);
        } finally {
            servicesTask1.closeAllFiles();
        }
    }

    private void doIt(String pointOfSaleListFilePath, int operationsCount, String... outFileNames) {

        servicesTask1.loadPointsOfSale(pointOfSaleListFilePath);

        int outFielesCount = outFileNames.length;

        if (outFielesCount == 0) {
            return;
        }

        int curYear = new GregorianCalendar().get(Calendar.YEAR);
        Calendar dateFrom = new GregorianCalendar(curYear - 1, 0, 1, 0, 0, 0);
        Calendar dateBy = new GregorianCalendar(curYear, 0, 1, 0, 0, 0);

        for (int i = 0; i < operationsCount; i++) {
            generateOperation(outFielesCount, dateFrom, dateBy, i, outFileNames);
        }


    }

    private void generateOperation(int outFielesCount, Calendar dateFrom, Calendar dateBy, int operationNumber, String[] outFileNames) {
        Calendar moment = new GregorianCalendar();
        moment.setTimeInMillis((long) ((Math.random() * (dateBy.getTimeInMillis() - dateFrom.getTimeInMillis() + 1)) + dateFrom.getTimeInMillis()));
        int pointOfSaleNumber = (int) ((Math.random() * servicesTask1.getPointsOfSaleAsList().size()) + 1);
        int number = operationNumber;
        double sum = (Math.random() * (90000.38 + 1) + 10000.12);
        Operation operation = new Operation(moment, servicesTask1.getPointOfSaleById(pointOfSaleNumber), number, sum);
        int сurFile = (int) (Math.random() * outFielesCount);
        servicesTask1.addToFile(outFileNames[сurFile], operation.toString());
    }
}

