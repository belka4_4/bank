package services;

import dao.DAO;
import model.PointOfSale;
import java.util.*;

public class Services_task1 extends Services {

    private DAO pointOfSaleDAO;

    public Services_task1(DAO pointOfSaleDAO) {
        this.pointOfSaleDAO = pointOfSaleDAO;
    }

    public void loadPointsOfSale(String filePath) {
        pointOfSaleDAO.loadFromFile(filePath);
    }

    public List<PointOfSale> getPointsOfSaleAsList() {
        return pointOfSaleDAO.getAllAsList();
    }

    public PointOfSale getPointOfSaleById(int number) {
        return (PointOfSale) pointOfSaleDAO.getByID(number);
    }

}
