package services;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class Services {

    private Map<String, PrintWriter> outFilesMap;

    public Services() {
        this.outFilesMap = new HashMap<String, PrintWriter>();
    }

    public void addToFile(String filePath, String value) {
        PrintWriter pw = outFilesMap.get(filePath);
        if (pw == null) {
            try {
                outFilesMap.put(filePath, new PrintWriter(filePath));
                pw = outFilesMap.get(filePath);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        pw.println(value);
    }

    public void closeAllFiles() {
        for (Map.Entry<String, PrintWriter> s : outFilesMap.entrySet()) {
            s.getValue().close();
        }
    }

}
