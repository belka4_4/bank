package services;

import model.Operation;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StatisticService {

    private Services_task2 services;

    public StatisticService(Services_task2 services) {
        this.services = services;
    }

    public void run(String statsDateFileName, String statsOfficesFileName, String... outFileNames) {
        try {
            doIt(statsDateFileName, statsOfficesFileName, outFileNames);
        } finally {
            services.closeAllFiles();
        }
    }

    private void doIt(String statsDateFileName, String statsOfficesFileName, String... inFileNames) {

        for (int i = 0; i < inFileNames.length; i++) {
            services.loadOperation(inFileNames[i]);
        }

        int outFielesCount = inFileNames.length;

        if (outFielesCount == 0) {
            return;
        }

        List<Operation> operationList = services.getOperationAsList();
        operationList.
                stream().
                collect(Collectors.
                        groupingBy((Operation o) -> LocalDate.of(
                                o.getMoment().get(Calendar.YEAR),
                                o.getMoment().get(Calendar.MONTH) + 1,
                                o.getMoment().get(Calendar.DAY_OF_MONTH)), Collectors.summingDouble(Operation::getSum))).
                entrySet().
                stream().
                sorted(Comparator.comparing((e) -> e.getKey())).
                forEach(i -> services.addToFile(statsDateFileName, i.toString()));

        operationList.
                stream().
                collect(Collectors.
                        groupingBy(Operation::getPointOfSale, Collectors.summingDouble(Operation::getSum))).
                entrySet().
                stream().
                sorted(Comparator.comparing((e) -> e.getKey().getNumber())).
                forEach(i -> services.addToFile(statsOfficesFileName, i.toString()));


    }

}
