package dao;

import exceptions.IllegalDataFormatEsception;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Map.Entry;

public class DAO<T, ID> {

    private final Map<ID, T> map = new TreeMap<>();
    private final Mapper<T, ID> mapper;

    public DAO(Mapper mapper) {
        this.mapper = mapper;
    }

    private T makeClone(final T entity) {
        return mapper.makeClone(entity);
    }

    public T getByID(ID id) {
        T entity = map.get(id);
        if (entity != null) {
            return map.get(id);
        } else {
            return null;
        }
    }

    public List<T> getAllAsList() {
        return map
                .entrySet()
                .stream()
                .map(Entry::getValue)
                .map(this::makeClone)
                .collect(Collectors.toList());
    }

    public void loadFromFile(String filePath) {
        try {
            Scanner scanner = new Scanner(new FileInputStream(filePath), "UTF-8");
            while (true) {
                String record = scanner.nextLine();
                if (record == null) break;
                T newEntity = mapper.map(record);
                ID id = mapper.getID(newEntity);
                if (newEntity != null) {
                    map.put(id, newEntity);
                }
            }
            scanner.close();
        } catch (FileNotFoundException f) {
            f.printStackTrace();
            System.err
                    .println("File " + filePath + " was not found. Stop application.");
            throw new RuntimeException(f);
        } catch (NoSuchElementException f) {
            //It doesn't work without it.
        } catch (IllegalDataFormatEsception illegalDataFormatEsception) {
            illegalDataFormatEsception.printStackTrace();
        }

    }

    public Mapper<T, ID> getMapper() {
        return mapper;
    }
}
