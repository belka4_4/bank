package dao;

import exceptions.IllegalDataFormatEsception;
import model.Operation;
import model.PointOfSale;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class OperationMapper implements Mapper<Operation, Long> {

    private DAO pointOfSaleDAO;

    public OperationMapper(DAO pointOfSaleDAO, String filePath) {
        this.pointOfSaleDAO = pointOfSaleDAO;
        pointOfSaleDAO.loadFromFile(filePath);
    }

    @Override
    public Operation map(String in) throws IllegalDataFormatEsception {
        if (in == null) {
            throw new IllegalDataFormatEsception("\n" + "Line cannot be null");
        }

        String[] operationInfo = in.split(",");
        if (operationInfo.length < 5) {
            throw new IllegalDataFormatEsception("\n" + "On the line: \"" + in + "\" is the wrong format for creating a pointOfSale.");
        }
        String date = operationInfo[0].trim() + " " + operationInfo[1].trim();
        String format = "dd/MM/yyyy hh:mm:ss";
        Calendar operationDate = new GregorianCalendar();
        try {
            operationDate.setTime(new SimpleDateFormat(format).parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        PointOfSale pointOfSale = (PointOfSale) pointOfSaleDAO.getByID(Integer.valueOf(operationInfo[2].trim()));
        int number = Integer.valueOf(operationInfo[3].trim());
        Double sum = Double.valueOf(operationInfo[4].trim());

        Operation value = new Operation(operationDate, pointOfSale, number, sum);
        return value;
    }

    @Override
    public Long getID(Operation entity) {
        return entity.getNumber();
    }

    @Override
    public Operation makeClone(Operation entity) {
        return new Operation(entity.getMoment(), entity.getPointOfSale(), entity.getNumber(), entity.getSum());
    }

}
