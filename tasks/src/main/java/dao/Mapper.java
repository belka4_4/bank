package dao;

import exceptions.IllegalDataFormatEsception;

public interface Mapper<T, ID> {

    public T map(String in) throws IllegalDataFormatEsception;

    public ID getID(T entity);

    public T makeClone(T entity);

}