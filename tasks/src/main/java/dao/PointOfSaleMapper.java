package dao;

import exceptions.IllegalDataFormatEsception;
import model.PointOfSale;

public class PointOfSaleMapper implements Mapper<PointOfSale, Integer> {
    @Override
    public PointOfSale map(String in) throws IllegalDataFormatEsception {
        if (in == null) {
            throw new IllegalDataFormatEsception("\n" + "Line cannot be null");
        }

        String[] pointOfSaleInfo = in.split("-");
        if (pointOfSaleInfo.length < 2) {
            throw new IllegalDataFormatEsception("\n" + "On the line: \"" + in + "\" is the wrong format for creating a pointOfSale.");
        }
        int number = Integer.valueOf(pointOfSaleInfo[0].trim());
        String address = pointOfSaleInfo[1].trim();

        PointOfSale value = new PointOfSale(number, address);
        return value;
    }

    @Override
    public Integer getID(PointOfSale entity) {
        return entity.getNumber();
    }

    @Override
    public PointOfSale makeClone(PointOfSale entity) {
        return new PointOfSale(entity.getNumber(), entity.getAddress());
    }
}
