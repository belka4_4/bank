package exceptions;

public class IllegalDataFormatEsception extends Exception {
    public IllegalDataFormatEsception() {
        super();
    }

    public IllegalDataFormatEsception(String message) {
        super(message);
    }

    public IllegalDataFormatEsception(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalDataFormatEsception(Throwable cause) {
        super(cause);
    }
}
