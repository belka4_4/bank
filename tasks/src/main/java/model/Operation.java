package model;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Operation {

    private Calendar moment;
    private PointOfSale pointOfSale;
    private long number;
    private double sum;

    public Operation(Calendar moment, PointOfSale pointOfSale, long number, double sum) {
        this.moment = moment;
        this.pointOfSale = pointOfSale;
        this.number = number;
        this.sum = sum;
    }

    public Calendar getMoment() {
        return moment;
    }

    public void setMoment(Calendar moment) {
        this.moment = moment;
    }

    public PointOfSale getPointOfSale() {
        return pointOfSale;
    }

    public void setPointOfSale(PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    @Override
    public String toString() {
        return
                new SimpleDateFormat("dd/MM/yyyy").format(moment.getTime()) +
                        "," + new SimpleDateFormat("hh:mm:ss").format(moment.getTime()) +
                        "," + pointOfSale.getNumber() +
                        "," + number +
                        "," + String.format("%.2f", sum);
    }
}
