package view;

import services.DataGenerationService;
import dao.DAO;
import dao.PointOfSaleMapper;
import services.Services_task1;

import java.util.Arrays;


public class RunApp {

    private static final Services_task1 SERVICES = new Services_task1(new DAO(new PointOfSaleMapper()));

    public static void main(String[] args) {
        if (args.length < 3) {
            System.out.println("Few parameters!");
            return;
        }

        new DataGenerationService(SERVICES).run(args[0], Integer.valueOf(args[1]), Arrays.copyOfRange(args, 2, args.length));
    }

}
