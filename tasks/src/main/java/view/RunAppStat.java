package view;

import services.StatisticService;
import dao.DAO;
import dao.OperationMapper;
import dao.PointOfSaleMapper;
import services.Services_task2;

import java.util.Arrays;

public class RunAppStat {

    public static void main(String[] args) {

        final DAO pointOfSaleDao = new DAO(new PointOfSaleMapper());
        final Services_task2 services = new Services_task2(new DAO(new OperationMapper(pointOfSaleDao, args[0])));

        if (args.length < 4) {
            System.out.println("Few parameters!");
            return;
        }

        new StatisticService(services).run(args[1], args[2], Arrays.copyOfRange(args, 3, args.length));
    }

}
